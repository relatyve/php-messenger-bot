<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 2017.10.05.
 * Time: 19:42
 */

// Mintagomba pimax

$servername = "localhost";
$username = "root";
$password = "fender";
$dbname = "bot";

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$log_session = rand(100000,999999);

$verify_token = "rewtwre432wer21efqwe"; // Verify token
$token = "EAAEzpZBHw9MMBAKffHHZBZBZBcIuij7gNwGRz6OrWANNsAbcOvG3XVZByukZBBZCsRgj0B49oa3lv3E24rSc8ScPqIBz9ttloZAmi8KzyTo27FpxNkJjENy23DTlZATPo9Tr3HSTSROO125tCWeAnpQZA5XMb8X0GYx1DfWlSVLA4oLgZDZD"; // Page token

$welcome_array = array("hi", "hy", "hello", "szia", "csao", "jónapot", "üdvözlöm", "menu");

if (file_exists(__DIR__ . '/config.php'))
    {
    $config = include __DIR__ . '/config.php';
    $verify_token = $config['verify_token'];
    $token = $config['token'];
    }

require_once(dirname(__FILE__) . '/vendor/autoload.php');

use pimax\FbBotApp;
use pimax\Menu\MenuItem;
use pimax\Menu\LocalizedMenu;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\MessageReceiptElement;
use pimax\Messages\Address;
use pimax\Messages\Summary;
use pimax\Messages\Adjustment;
use pimax\Messages\AccountLink;
use pimax\Messages\ImageMessage;
use pimax\Messages\QuickReply;
use pimax\Messages\QuickReplyButton;
use pimax\Messages\SenderAction;
use pimax\Messages\FileMessage;
use pimax\Messages\AudioMessage;
use pimax\Messages\VideoMessage;
use pimax\Messages\Attachment;
use pimax\Messages\GreetengMessage;

function db_logging($conn, $data, $mtype, $m, $q, $sess)
    {
    $stmt = $conn->prepare("INSERT INTO log (content, date, owner, messagetype, message, querytype, session) VALUES (:content, :date, :owner, :messagetype, :message, :querytype, :session)");
    $stmt->bindParam(':content', $content);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':owner', $owner);
    $stmt->bindParam(':messagetype', $messagetype);
    $stmt->bindParam(':message', $message);
    $stmt->bindParam(':querytype', $querytype);
    $stmt->bindParam(':session', $session);

    // insert a row
    $content = $data;
    $date = date("Y-m-d H:i:s");
    $owner = "-";
    $messagetype = $mtype;
    $message = $m;
    $querytype = $q;
    $session = $sess;
    $stmt->execute();
    }


    // -----------------------------------------------------------------------------
//    $welcome_StructuredMessage = new StructuredMessage($message_sender_id,
//                                            StructuredMessage::TYPE_BUTTON,
//                                            [
//                                                'text' => "Üdvözöllek a gyógy-,wellness-, és konferencia szállodánkban!",
//                                                'buttons' =>
//                                                    [
//                                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
//                                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Wi-Fi jelszó', 'PAYLOAD-MAIN-WIFI'),
//                                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Helyi látnivalók', 'PAYLOAD-MAIN-LAT')
//                                                    ]
//                                            ]);

    // -----------------------------------------------------------------------------


function abstractMessage($botm, $sm, $token)
    {
    $getmessage = $sm->getData();
    $getmessage['access_token'] = $token;
    $botm->send($sm);
    }


function welcome($botm, $message_sender_id, $conn, $log_session, $token)
    {
    $message_title = "!!! Üdvözöllek a gyógy-,wellness-, és konferencia szállodánkban! Én leszek az inasod, aki segít kellemessé tenni a nálunk töltött időt. /kép/ Oda tudom adni a wifi jelszót, megmutatom a hotel szolgáltatásait, valamint, hogy mit érdemes megnézned a környéken. Mivel kezdjük?";

    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
            [
            'text' => $message_title,
            'buttons' =>
                [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Wi-Fi jelszó', 'PAYLOAD-MAIN-WIFI'),
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Helyi látnivalók', 'PAYLOAD-MAIN-LAT')
                ]
            ]
    );

    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;

        db_logging($conn, json_encode($getmessage), "send", "-", "response", $log_session);

    $botm->send($msg);
    }

//function teszt($botm, $message_sender_id, $conn, $log_session, $token)
//    {
//    $teszt_message = 'Teszteles......';
//    $msg = new Message($message_sender_id, $teszt_message);
//    $getmessage = $msg->getData();
//    $getmessage['access_token'] = $token;
//    $botm->send($msg);
//    }


// Make Bot Instance
$bot = new FbBotApp($token);


if (!empty($_REQUEST['local']))
    {

    $message = new ImageMessage(1585388421775947, dirname(__FILE__).'/fb4d_logo-2x.png');
    $message_data = $message->getData();
    $message_data['message']['attachment']['payload']['url'] = 'fb4d_logo-2x.png';
    echo '<pre>', print_r($message->getData()), '</pre>';
    $res = $bot->send($message);
    echo '<pre>', print_r($res), '</pre>';
    }

// Receive something
if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token)
    {
    // Webhook setup request
    echo $_REQUEST['hub_challenge'];
    }
else
    {
    // Other event
    $data_raw = file_get_contents("php://input");

    $data = json_decode($data_raw, true, 512, JSON_BIGINT_AS_STRING);



    if (!empty($data['entry'][0]['messaging']))
        {

            // ******************** Sender ID **************
            $sender_id = $data['entry'][0]['messaging'][0]['sender']['id'];
            // ******************** Sender ID **************

            // **********************************************************************************************
            // ***************************************** CONTENTS *******************************************
            // **********************************************************************************************
            $wmsg = new StructuredMessage($sender_id,
                StructuredMessage::TYPE_BUTTON,
                                            [
                                                'text' => "Teszteles uzenet...",
                                                'buttons' =>
                                                    [
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Wi-Fi jelszó', 'PAYLOAD-MAIN-WIFI'),
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Helyi látnivalók', 'PAYLOAD-MAIN-LAT')
                                                    ]
                                            ]
            );

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            $generic = new StructuredMessage($sender_id,
                                                StructuredMessage::TYPE_GENERIC,
                                                [
                                                    'elements' => [
                                                        new MessageElement("First item", "Next-generation virtual reality", "www.index.hu","https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/gearvrsq.png", [
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'Megtekint')
                                                        ]),

                                                        new MessageElement("Second item", "Item description", "www.index.hu","https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/riftsq.png", [
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'First button')
                                                        ]),

                                                        new MessageElement("Third item", "Item description", "www.index.hu","https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/touch.png", [
                                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'First button')
                                                        ])
                                                    ]
                                                ],
                                                [
                                                    new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
                                                ]
                                            );
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $textmessage = new Message($sender_id, "Ez egy sima üzenet.... ");

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $list = new StructuredMessage($sender_id,
                StructuredMessage::TYPE_LIST,
                                        [
                                            'elements' => [
                                                new MessageElement(
                                                    'Classic T-Shirt Collection', // title
                                                    'See all our colors', // subtitle
                                                    'http://bit.ly/2pYCuIB', // image_url
                                                    [ // buttons
                                                        new MessageButton(MessageButton::TYPE_POSTBACK, // type
                                                            'View', // title
                                                            'POSTBACK' // postback value
                                                        )
                                                    ]
                                                ),
                                                new MessageElement(
                                                    'Classic White T-Shirt', // title
                                                    '100% Cotton, 200% Comfortable', // subtitle
                                                    'http://bit.ly/2pb1hqh', // image_url
                                                    [ // buttons
                                                        new MessageButton(MessageButton::TYPE_WEB, // type
                                                            'View', // title
                                                            'https://google.com' // url
                                                        )
                                                    ]
                                                )
                                            ],
                                            'buttons' => [
                                                new MessageButton(MessageButton::TYPE_POSTBACK, 'First button', 'PAYLOAD 1')
                                            ]
                                        ],
                                        [
                                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
                                        ]
                                    );

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $image = new ImageMessage($sender_id, 'http://bit.ly/2p9WZBi');


            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $file = new FileMessage($sender_id, 'https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/test.txt');


            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $video = new VideoMessage($sender_id, 'https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/allofus480.mov');

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $audio = new AudioMessage($sender_id, 'https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/sample.mp3');

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            $gif = new ImageMessage($sender_id, 'https://media.giphy.com/media/11sBLVxNs7v6WA/giphy.gif');

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


            $grt = new GreetengMessage($sender_id, "Ez egy sima üzenet.... ");

            // **********************************************************************************************
            // ***************************************** CONTENTS *******************************************
            // **********************************************************************************************





        foreach ($data['entry'][0]['messaging'] as $message)
            {


            // Skipping delivery messages
            if (!empty($message['delivery']))
                {
                continue;
                }


            $command = "";

            // When bot receive message from user
            if (!empty($message['message']))
                {
                $command = trim($message['message']['text']);

                db_logging($conn, $data_raw, "text", $command, "request", $log_session);

                }
            else if (!empty($message['postback']))
                {


                db_logging($conn, $data_raw, "postback", $message['postback']['payload'], "request", $log_session);

                switch ($message['postback']['payload'])
                    {

                    case 'PAYLOAD - get started button':
                //        $grt = new GreetengMessage($message['sender']['id'], "Ez egy greeting  üzenet.... {{user_full_name}}");
                //        abstractMessage($bot, $grt, $token);

                        $gr = ["setting_type" => "greeting",
                                "greeting" =>
                                            [
                                            "locale" => "default",
                                            "text" => "aaabbccc"
                                            ]
                                ];

                        //*************** work *******************
                        /*
                        $gr = ["recipient"=>
                                            [
                                            "id" => $message['sender']['id']
                                            ],
                               "message"=>
                                            [
                                            "text" => "hello, world!"
                                            ]
                                ];
                        */

                        $getmessage = $gr;
                        $getmessage['access_token'] = $token;
                        $bot->sendArray($gr);

                //        var_dump( json_encode($gr));

                        break;

                    case 'PAYLOAD-MAIN-SZOLG':
                        szolgaltatasaink($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-WIFI':
                        wifiPass($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-LAT':
                        helyiLatnivalok($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;




                    }

                continue;
                }

            // ***************************** 2017-10-01 ***************************************

            $comm = strtolower($command);

            if(in_array($comm, $welcome_array))
                {
                welcome($bot, $message['sender']['id'], $conn, $log_session, $token);

                // welcome($botm, $message_sender_id, /*$logfile,*/ $conn, $log_session, $token)

                continue;
                }

            // ***************************** 2017-10-01 ***************************************

            // Handle command
            switch ($command)
                {
                case "aaa";
             //       teszt($bot, $sender_id, $conn, $log_session, $token);
                    abstractMessage($bot, $wmsg, $token);
                    break;
                case "generic":
                    abstractMessage($bot, $generic, $token);
                    break;
                case "textmessage":
                    abstractMessage($bot, $textmessage, $token);
                    break;
                case "list":
                    abstractMessage($bot, $list, $token);
                    break;
                case "image":
                    abstractMessage($bot, $image, $token);
                    break;
                case "file":
                    abstractMessage($bot, $file, $token);
                    break;
                case "video":
                    abstractMessage($bot, $video, $token);
                    break;
                case "audio":
                    abstractMessage($bot, $audio, $token);
                    break;
                case "gif":
                    abstractMessage($bot, $gif, $token);
                    break;
                case "greeteing":
                    abstractMessage($bot, $grt, $token);
                    break;

                // Other message received
                default:
                    if (!empty($command)) // otherwise "empty message" wont be understood either
                        $bot->send(new Message($message['sender']['id'], "Sajnálom, nem ismerem fel amit írtál... Használd kérlek az alábbi szavakat: \nhi \nhy \nhello \nszia, \ncsao \njónapot \nüdvözlöm \nmenu"));
                }
            }
        }
    }