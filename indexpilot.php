<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 2017.09.19.
 * Time: 21:06
 */

// ************

$servername = "localhost";
$username = "root";
$password = "fender";
$dbname = "bot";

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



function db_logging($conn, $data, $mtype, $m, $q, $sess)
    {
        $stmt = $conn->prepare("INSERT INTO log (content, date, owner, messagetype, message, querytype, session) VALUES (:content, :date, :owner, :messagetype, :message, :querytype, :session)");
        $stmt->bindParam(':content', $content);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':owner', $owner);
        $stmt->bindParam(':messagetype', $messagetype);
        $stmt->bindParam(':message', $message);
        $stmt->bindParam(':querytype', $querytype);
        $stmt->bindParam(':session', $session);

        // insert a row
        $content = $data;
        $date = date("Y-m-d H:i:s");
        $owner = "-";
        $messagetype = $mtype;
        $message = $m;
        $querytype = $q;
        $session = $sess;
        $stmt->execute();
    }


// ************




// -------------- log -------------
$log_session = rand(100000,999999);
$logfile = 'C:/DEV/wamp64/www/botproject/log/bot.log';
////// $current = file_get_contents($logfile);

// file_put_contents($logfile, "\nstart bot... \n", FILE_APPEND | LOCK_EX);

// -------------- log -------------

// ********************************************
function logging($logfile, $data, $log_session)
    {
///////    file_put_contents($logfile, $log_session . " | " . $data . " \n", FILE_APPEND | LOCK_EX);
    }

function sendmessage($botm, $msg, $log_session, $token, $conn)
    {
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
 //   logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);

        db_logging($conn, json_encode($getmessage), "send", "-", "response", $log_session);

    $botm->send($msg);
    }

// ********************************************

logging($logfile, "Start app...", $log_session);


// ---------------------------------------------------------------------
/*
buttonlist
text
image
profile
button
quick reply
location
generic
list
receipt
set menu
delete menu
logout
sender action on
sender action off
set get started button
delete get started button
show greeting text
delete greeting text
set greeting text
show target audience
set target audience
delete target audience
show domain whitelist
set domain whitelist
delete domain whitelist
*/
// ---------------------------------------------------------------------


$verify_token = "rewtwre432wer21efqwe"; // Verify token
$token = "EAAEzpZBHw9MMBAKffHHZBZBZBcIuij7gNwGRz6OrWANNsAbcOvG3XVZByukZBBZCsRgj0B49oa3lv3E24rSc8ScPqIBz9ttloZAmi8KzyTo27FpxNkJjENy23DTlZATPo9Tr3HSTSROO125tCWeAnpQZA5XMb8X0GYx1DfWlSVLA4oLgZDZD"; // Page token


if (file_exists(__DIR__ . '/config.php'))
{
    $config = include __DIR__ . '/config.php';
    $verify_token = $config['verify_token'];
    $token = $config['token'];
}

require_once(dirname(__FILE__) . '/vendor/autoload.php');

use pimax\FbBotApp;
use pimax\Menu\MenuItem;
use pimax\Menu\LocalizedMenu;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\MessageReceiptElement;
use pimax\Messages\Address;
use pimax\Messages\Summary;
use pimax\Messages\Adjustment;
use pimax\Messages\AccountLink;
use pimax\Messages\ImageMessage;
use pimax\Messages\QuickReply;
use pimax\Messages\QuickReplyButton;
use pimax\Messages\SenderAction;


// *************** text config ************************

$welcome_array = array("hi", "hy", "hello", "szia", "csao", "jónapot", "üdvözlöm", "menu");;

$welcome_message = "Üdvözöllek a gyógy-,wellness-, és konferencia szállodánkban! Én leszek az inasod, aki segít kellemessé tenni a nálunk töltött időt. /kép/ Oda tudom adni a wifi jelszót, megmutatom a hotel szolgáltatásait, valamint, hogy mit érdemes megnézned a környéken. Mivel kezdjük?";

function welcome($botm, $message_sender_id, /*$logfile,*/ $conn, $log_session, $token)
    {
        $message_title = "Üdvözöllek a gyógy-,wellness-, és konferencia szállodánkban! Én leszek az inasod, aki segít kellemessé tenni a nálunk töltött időt. /kép/ Oda tudom adni a wifi jelszót, megmutatom a hotel szolgáltatásait, valamint, hogy mit érdemes megnézned a környéken. Mivel kezdjük?";

        $msg = new StructuredMessage($message_sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => $message_title,
                'buttons' => [
                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Wi-Fi jelszó', 'PAYLOAD-MAIN-WIFI'),
                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Helyi látnivalók', 'PAYLOAD-MAIN-LAT')
                ]
            ]
        );

        $getmessage = $msg->getData();
        $getmessage['access_token'] = $token;
    //    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);


        db_logging($conn, json_encode($getmessage), "send", "-", "response", $log_session);

        $botm->send($msg);
    }


// ********* WiFi content ************
function wifiPass($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $wifi_message = 'A WIFI jelszó: 123456789000';
    $msg = new Message($message_sender_id, $wifi_message);
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* WiFi content ************

// ********* Helyi latnivalok content ************
function helyiLatnivalok($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $message_title = "Közelünkben található a falumúzeum, mellette egy kis horgásztó, mely azoknak biztosít programot, akik a rohanó városi élet forgatagából kívánnak kilépni a csend és a nyugalom világába. \n Weboldal: https://silvermajor.hu>";

    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Múzeum', 'PAYLOAD-MAIN-MUZEUM'),
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Horgászati lehetőség', 'PAYLOAD-MAIN-HORGASZATI')
            ]
        ]
    );

    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* Helyi latnivalok content ************

// ********* Muzeum ******************************
function muzeum($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $wifi_message = 'Múzeum tartalom...';
    $msg = new Message($message_sender_id, $wifi_message);
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* Muzeum ******************************

// ********* Horgaszati lehetosegek ******************************
function horgaszati_lehetosegek($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $wifi_message = 'Horgászati lehetőségek...';
    $msg = new Message($message_sender_id, $wifi_message);
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* Horgaszati lehetosegek ******************************

// ********* Szolgaltatasaink ************************************

function szolgaltatasaink($botm, $message_sender_id, $logfile, $log_session, $token)
    {

    $message_title = "Nagy örömömre szolgál, hogy Vendégünkként üdvözölhetlek szállodánkban, amely a keleti régió egyik legszínvonalasabb gyógy- wellness és konferencia szállodája! Megmutatok mindent, ami érdekelhet Téged! Merre induljunk?";

    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Strand', 'PAYLOAD-MAIN-STRAND'),
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Wellness', 'PAYLOAD-MAIN-WELLNESS'),
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Szabadidőközpont', 'PAYLOAD-MAIN-SZABADIDOKOZPONT'),
//                new MessageButton(MessageButton::TYPE_POSTBACK, 'Gaszt', 'PAYLOAD-MAIN-GASZTRO')
            ]
        ]
    );

    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);

    }
// ********* Szolgaltatasaink ************************************


// ********* strand ******************************
function strand($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $wifi_message = 'Szállodánk -Magyarországon egyedülállóan- 2 saját stranddal rendelkezik, mindkét épületrészünkben található fürdőkomplexum. A 10 medence, a görög és olasz napozóágyak (a napi igény 3-szoros mennyiségével), a vizibár, a vendéglátó egységek és a személyzet mind a Te kényelmét szolgálja. A gyerekekről sem feledkezünk meg, játszóház és gyermekmedence áll rendelkezésükre! Bébi,-bababarát, mindennap tisztított, vegyszermentes medencében várjuk kedves Gyermekvendégeinket.';
    $msg = new Message($message_sender_id, $wifi_message);
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* strand ******************************

// ********* strand ******************************
function szabadidokozpont($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $message_title = 'Szállodánkban rossz idő esetén sem fogsz unatkozni. Kétpályás bowling, versenycsocsó, biliárd, asztalitenisz, stratégiai játékok, szimulátorok járulnak hozzá a kellemes kikapcsolódásához. Nyitvatartás: Minden nap 10:00-24:00, Pályafoglalás vagy érdeklődés: +36 11/222 333';
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_CALL, 'Hívás', '06201234567'),
            ]
        ]
    );

    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* strand ******************************

// ********* wellness ****************************
function wellness($botm, $message_sender_id, $logfile, $log_session, $token)
    {

    $message_title = "Az általános jó közérzet, a teljes testi és lelki kiegyensúlyozottság programja. A mai kor embere számára kialakított reform-életforma, amely magában foglalja a fittséget és a testi, lelki, szellemi egészséget, ugyanakkor azoknál jóval többet jelent. Míg a testi egészség a betegségmentes fizikai állapot, addig a fittség megnövekedett teljesítőképességet jelez, mely kihat a mindennapi munkavégzésre. A wellness tehát nemcsak a testi, de a lelki, érzelmi, szellemi teljesítő- és tűrőképesség fokozódását jelenti, és fizikális vonatkozása mellett a tökéletes közérzethez nélkülözhetetlen mentális és pszichés faktorokat is tartalmazza.";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Sókamra', 'PAYLOAD-MAIN-SOKAMRA'),
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);

    // ------------------------------------------------------
    $message_title = "Időpont egyeztetés a szálloda elérhetőségén:Telefon: +36 11/222-333 /145-es mellék vagy XY: 06 30/000 00 00";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Fodrászat', 'PAYLOAD-MAIN-FODRASZAT'),
                new MessageButton(MessageButton::TYPE_CALL, 'Hívás', '0123456789')
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    // -------------------------------------------------------
    $message_title = "Időpont egyeztetés a szálloda elérhetőségén:Telefon: +36 11/222-333/145 /182-es mellék vagy XY-nál a +36 20 000 00 00-as telefonszámon";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Manikűr Pedikűr Masszázs', 'PAYLOAD-MAIN-MAIKUR'),
                new MessageButton(MessageButton::TYPE_CALL, 'Hívás', '0123456789')
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    // ------------------------------------------------------
    $message_title = "Bodylizer System";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Bodylizer System', 'PAYLOAD-MAIN-BODYLIZER'),
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);

    }
// ********* wellenss ****************************

// ********* bodylizer ***************************
function bodylizer($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $message_title = "A bodylizer System a legújabb generációs arc,- és testkezelő elektromos készülék, amiben ultrahang, mikroimpulzus és lézertechnika is szerepel. Mindez orvosi beavatkozás nélkül. Ez a multifunkcionális gép megfiatalítja a bőr állományát és a testet eredeti formájára alakítja. A bodylizer System a legújabb számítógépes technikát alkalmazza, amellyel impulzusokat szimulál, amit a szervezet saját maga produkál az idegeken keresztül. Ezzel regeneráló ingereket küldünk a sejtekhez és a belső szervekhez, mely hosszan tartó hatást vált ki.";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* bodylizer ***************************


// ********* fodraszat ***************************
function fodraszat($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $message_title = "Fodrász szalonunk teljes körű szolgáltatással várja vendégeit.";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* fodraszat ***************************

// ********* manikur ***************************
function mainkur($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $message_title = "https://hotelsilver.hu/wellness";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* manikur ***************************

// ********* sokamra ***************************
function sokamra($botm, $message_sender_id, $logfile, $log_session, $token)
    {
    $message_title = "Évszázadok óta ismert, hogy a természetes sóbarlangok klímája és a sós tengeri levegő, gyógyító hatást gyakorol az emberi szervezetre, főként a különféle légúti és légzőszervi betegségekre, a bőr és izületi problémákra. A só terápia egy méregtelenítő gyógymód.";
    $msg = new StructuredMessage($message_sender_id,
        StructuredMessage::TYPE_BUTTON,
        [
            'text' => $message_title,
            'buttons' => [
                new MessageButton(MessageButton::TYPE_POSTBACK, 'Szolgáltatásaink', 'PAYLOAD-MAIN-SZOLG'),
            ]
        ]
    );
    $getmessage = $msg->getData();
    $getmessage['access_token'] = $token;
    logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);
    $botm->send($msg);
    }
// ********* sokamra ***************************

// *************** text config ************************

// Make Bot Instance
$bot = new FbBotApp($token);

if (!empty($_REQUEST['local']))
    {

    $message = new ImageMessage(1585388421775947, dirname(__FILE__).'/fb4d_logo-2x.png');

    $message_data = $message->getData();
    $message_data['message']['attachment']['payload']['url'] = 'fb4d_logo-2x.png';

    echo '<pre>', print_r($message->getData()), '</pre>';

    $res = $bot->send($message);

    echo '<pre>', print_r($res), '</pre>';
    }

// Receive something
if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token)
    {
    logging($logfile, "Webhook setup request", $log_session);
    // Webhook setup request
    echo $_REQUEST['hub_challenge'];
    }
else
    {
    logging($logfile, "Other event 1 2 3", $log_session);
    // Other event

    $data_raw = file_get_contents("php://input");

    $data = json_decode($data_raw, true, 512, JSON_BIGINT_AS_STRING);

    logging($logfile, "REQUEST --> " . $data_raw, $log_session);



    if (!empty($data['entry'][0]['messaging']))
        {



        foreach ($data['entry'][0]['messaging'] as $message)
            {

            // Skipping delivery messages
            if (!empty($message['delivery']))
                {
                continue;
                }

            // skip the echo of my own messages
            /*
            if (($message['message']['is_echo'] == "true"))
                {
                continue;
                }
            */
            $command = "";

            // When bot receive message from user
            if (!empty($message['message']))
                {
                $command = trim($message['message']['text']);
                logging($logfile, "When bot receive message from user - command = " . $command, $log_session);

                    db_logging($conn, $data_raw, "text", $command, "request", $log_session);

                }
            else if (!empty($message['postback']))
                {

                logging($logfile, "POSTBACK............... -> " . $message['postback']['payload'], $log_session);

                    db_logging($conn, $data_raw, "postback", $message['postback']['payload'], "request", $log_session);

                switch ($message['postback']['payload'])
                    {

                    case 'PAYLOAD-MAIN-SZOLG':
                        szolgaltatasaink($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-WIFI':
                        wifiPass($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-LAT':
                        helyiLatnivalok($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-MUZEUM':
                        muzeum($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-HORGASZATI':
                        horgaszati_lehetosegek($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-STRAND':
                        strand($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-WELLNESS':
                        wellness($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-SZABADIDOKOZPONT':
                        szabadidokozpont($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-GASZTRO':

                        break;

                    case 'PAYLOAD-MAIN-SOKAMRA':
                        sokamra($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-FODRASZAT':
                        fodraszat($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-MANIKUR':
                        mainkur($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;

                    case 'PAYLOAD-MAIN-BODYLIZER':
                        bodylizer($bot, $message['sender']['id'], $logfile, $log_session, $token);
                        break;
                    }

                continue;
                }

            // ***************************** 2017-10-01 ***************************************

            $comm = strtolower($command);

            if(in_array($comm, $welcome_array))
                {
                welcome($bot, $message['sender']['id'], /*$logfile,*/ $conn, $log_session, $token);

                // welcome($botm, $message_sender_id, /*$logfile,*/ $conn, $log_session, $token)

                continue;
                }

            // ***************************** 2017-10-01 ***************************************

            // Handle command
            switch ($command)
                {
                case "aa";

                    break;


                // Other message received
                default:
                    if (!empty($command)) // otherwise "empty message" wont be understood either
                        $bot->send(new Message($message['sender']['id'], "Sajnálom, nem ismerem fel amit írtál... Használd kérlek az alábbi szavakat: \nhi \nhy \nhello \nszia, \ncsao \njónapot \nüdvözlöm \nmenu"));
            }
        }
    }
}