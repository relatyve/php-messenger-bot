<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 2017.09.19.
 * Time: 21:06
 */
// -------------- log -------------
$log_session = rand(100000,999999);
$logfile = 'C:/DEV/wamp64/www/botproject/log/bot.log';
$current = file_get_contents($logfile);

// file_put_contents($logfile, "\nstart bot... \n", FILE_APPEND | LOCK_EX);

// -------------- log -------------

// ********************************************
function logging($logfile, $data, $log_session)
    {
    file_put_contents($logfile, $log_session . " | " . $data . " \n", FILE_APPEND | LOCK_EX);
    }

function sendmessage($botm, $msg, $logfile, $log_session, $token)
    {

        $getmessage = $msg->getData();
        $getmessage['access_token'] = $token;
        logging($logfile, "SEND Message = " . json_encode($getmessage), $log_session);

        $botm->send($msg);
    }

// ********************************************

logging($logfile, "Start app...", $log_session);


// ---------------------------------------------------------------------
/*
buttonlist
text
image
profile
button
quick reply
location
generic
list
receipt
set menu
delete menu
logout
sender action on
sender action off
set get started button
delete get started button
show greeting text
delete greeting text
set greeting text
show target audience
set target audience
delete target audience
show domain whitelist
set domain whitelist
delete domain whitelist
*/
// ---------------------------------------------------------------------



$verify_token = "rewtwre432wer21efqwe"; // Verify token
$token = "EAAEzpZBHw9MMBAKffHHZBZBZBcIuij7gNwGRz6OrWANNsAbcOvG3XVZByukZBBZCsRgj0B49oa3lv3E24rSc8ScPqIBz9ttloZAmi8KzyTo27FpxNkJjENy23DTlZATPo9Tr3HSTSROO125tCWeAnpQZA5XMb8X0GYx1DfWlSVLA4oLgZDZD"; // Page token





if (file_exists(__DIR__ . '/config.php'))
{
    $config = include __DIR__ . '/config.php';
    $verify_token = $config['verify_token'];
    $token = $config['token'];
}

require_once(dirname(__FILE__) . '/vendor/autoload.php');

use pimax\FbBotApp;
use pimax\Menu\MenuItem;
use pimax\Menu\LocalizedMenu;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\MessageReceiptElement;
use pimax\Messages\Address;
use pimax\Messages\Summary;
use pimax\Messages\Adjustment;
use pimax\Messages\AccountLink;
use pimax\Messages\ImageMessage;
use pimax\Messages\QuickReply;
use pimax\Messages\QuickReplyButton;
use pimax\Messages\SenderAction;


// Make Bot Instance
$bot = new FbBotApp($token);

if (!empty($_REQUEST['local']))
{

    $message = new ImageMessage(1585388421775947, dirname(__FILE__).'/fb4d_logo-2x.png');

    $message_data = $message->getData();
    $message_data['message']['attachment']['payload']['url'] = 'fb4d_logo-2x.png';

    echo '<pre>', print_r($message->getData()), '</pre>';

    $res = $bot->send($message);

    echo '<pre>', print_r($res), '</pre>';
}

// Receive something
if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token)
{
 /////   file_put_contents($logfile, "Webhook setup request \n", FILE_APPEND | LOCK_EX);
     logging($logfile, "Webhook setup request", $log_session);
    // Webhook setup request
    echo $_REQUEST['hub_challenge'];
}
else
{
  //  file_put_contents($logfile, "Other event 1 2 3 \n", FILE_APPEND | LOCK_EX);
    logging($logfile, "Other event 1 2 3", $log_session);
    // Other event

    $data_raw = file_get_contents("php://input");

    $data = json_decode($data_raw, true, 512, JSON_BIGINT_AS_STRING);

 //   file_put_contents($logfile, "REQUEST --> " . $data_raw . "\n", FILE_APPEND | LOCK_EX);

    logging($logfile, "REQUEST --> " . $data_raw, $log_session);

    if (!empty($data['entry'][0]['messaging']))
    {
        foreach ($data['entry'][0]['messaging'] as $message)
        {

            // Skipping delivery messages
            if (!empty($message['delivery']))
            {
                continue;
            }

            // skip the echo of my own messages
            /*
            if (($message['message']['is_echo'] == "true"))
                {
                continue;
                }
            */
            $command = "";

            // When bot receive message from user
            if (!empty($message['message']))
                {
                $command = trim($message['message']['text']);

      //          file_put_contents($logfile, "When bot receive message from user - command = " . $command . "\n", FILE_APPEND | LOCK_EX);
                    logging($logfile, "When bot receive message from user - command = " . $command, $log_session);
                // When bot receive button click from user
                }
            else if (!empty($message['postback']))
                {
                $text = "Postback received: ".trim($message['postback']['payload']);
                $bot->send(new Message($message['sender']['id'], $text));
                continue;
                }

            // Handle command
            switch ($command)
            {

                case 'buttonlist':
                    file_put_contents($logfile, "switch - command = buttonlist \n", FILE_APPEND | LOCK_EX);
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_BUTTON,
                        [
                            'text' => 'Főmenü',
                            'buttons' => [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'First button', 'PAYLOAD 1'),
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Second button', 'PAYLOAD 2'),
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Third button', 'PAYLOAD 3')
                            ]
                        ]
                    ));
                    break;
                // When bot receive "text"
                case 'text':
                ///    file_put_contents($logfile, "switch - command = text \n", FILE_APPEND | LOCK_EX);
                    sendmessage($bot, new Message($message['sender']['id'], 'This is a simple text message........'), $logfile, $log_session, $token);
                //    sendmessage($botm, $msg, $logfile, $log_session, $token)
                //    $bot->send(new Message($message['sender']['id'], 'This is a simple text message.'));
                    break;

                // When bot receive "image"
                case 'image':
                //    file_put_contents($logfile, "switch - command = image \n", FILE_APPEND | LOCK_EX);
                    $bot->send(new ImageMessage($message['sender']['id'], 'http://bit.ly/2p9WZBi'));
                    break;

                // When bot receive "local image"
                //case 'local image':
                //$bot->send(new ImageMessage($message['sender']['id'], dirname(__FILE__).'/fb_logo.png'));
                //break;

                // When bot receive "profile"
                case 'profile':
                    $user = $bot->userProfile($message['sender']['id']);
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_GENERIC,
                        [
                            'elements' => [
                                new MessageElement($user->getFirstName()." ".$user->getLastName(), " ", $user->getPicture())
                            ]
                        ],
                        [
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
                        ]
                    ));
                    break;

                // When bot receive "button"
                case 'button':

                    $message = new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_BUTTON,
                        [
                            'text' => 'Choose category',
                            'buttons' => [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'First button', 'PAYLOAD 1'),
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Second button', 'PAYLOAD 2'),
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Third button', 'PAYLOAD 3')
                            ]
                        ],
                        [
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
                        ]
                    );

//                    $getmessage = $message->getData();
//                    $getmessage['access_token'] = $token;
//                    logging($logfile, "Message = " . json_encode($getmessage), $log_session);


//                    $bot->send(new StructuredMessage($message['sender']['id'],
//                        StructuredMessage::TYPE_BUTTON,
//                        [
//                            'text' => 'Choose category',
//                            'buttons' => [
//                                new MessageButton(MessageButton::TYPE_POSTBACK, 'First button', 'PAYLOAD 1'),
//                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Second button', 'PAYLOAD 2'),
//                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Third button', 'PAYLOAD 3')
//                            ]
//                        ],
//                        [
//                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
//                        ]
//                    ));

                    sendmessage($bot, $message, $logfile, $log_session, $token);
                    break;

                // When bot receive "quick reply"
                case 'quick reply':
                    $bot->send(new QuickReply($message['sender']['id'], 'Your ad here!',
                        [
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button 1', 'PAYLOAD 1'),
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button 2', 'PAYLOAD 2'),
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button 3', 'PAYLOAD 3'),
                        ]
                    ));
                    break;

                // When bot receive "location"
                case 'location':
                    $bot->send(new QuickReply($message['sender']['id'], 'Please share your location',
                        [
                            new QuickReplyButton(QuickReplyButton::TYPE_LOCATION),
                        ]
                    ));
                    break;

                // When bot receive "generic"
                case 'generic':
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_GENERIC,
                        [
                            'elements' => [
                                new MessageElement("First item", "Item description", "", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'First button'),
                                    new MessageButton(MessageButton::TYPE_WEB, 'Web link', 'http://facebook.com')
                                ]),

                                new MessageElement("Second item", "Item description", "", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'First button'),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Second button')
                                ]),

                                new MessageElement("Third item", "Item description", "", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'First button'),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Second button')
                                ])
                            ]
                        ],
                        [
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
                        ]
                    ));
                    break;

                case 'genericcustom':
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_GENERIC,
                        [
                            'elements' => [
                                new MessageElement("First item", "Next-generation virtual reality", "www.index.hu","https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/gearvrsq.png", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Megtekint')
                                ]),

                                new MessageElement("Second item", "Item description", "www.index.hu","https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/riftsq.png", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'First button')
                                ]),

                                new MessageElement("Third item", "Item description", "www.index.hu","https://raw.githubusercontent.com/fbsamples/messenger-platform-samples/master/node/public/assets/touch.png", [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'First button')
                                ])
                            ]
                        ],
                        [
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
                        ]
                    ));
                    break;


                // When bot receive "list"
                case 'list':
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_LIST,
                        [
                            'elements' => [
                                new MessageElement(
                                    'Classic T-Shirt Collection', // title
                                    'See all our colors', // subtitle
                                    'http://bit.ly/2pYCuIB', // image_url
                                    [ // buttons
                                        new MessageButton(MessageButton::TYPE_POSTBACK, // type
                                            'View', // title
                                            'POSTBACK' // postback value
                                        )
                                    ]
                                ),
                                new MessageElement(
                                    'Classic White T-Shirt', // title
                                    '100% Cotton, 200% Comfortable', // subtitle
                                    'http://bit.ly/2pb1hqh', // image_url
                                    [ // buttons
                                        new MessageButton(MessageButton::TYPE_WEB, // type
                                            'View', // title
                                            'https://google.com' // url
                                        )
                                    ]
                                )
                            ],
                            'buttons' => [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'First button', 'PAYLOAD 1')
                            ]
                        ],
                        [
                            new QuickReplyButton(QuickReplyButton::TYPE_TEXT, 'QR button','PAYLOAD')
                        ]
                    ));
                    break;

                // When bot receive "receipt"
                case 'receipt':
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_RECEIPT,
                        [
                            'recipient_name' => 'Fox Brown',
                            'order_number' => rand(10000, 99999),
                            'currency' => 'USD',
                            'payment_method' => 'VISA',
                            'order_url' => 'http://facebook.com',
                            'timestamp' => time(),
                            'elements' => [
                                new MessageReceiptElement("First item", "Item description", "", 1, 300, "USD"),
                                new MessageReceiptElement("Second item", "Item description", "", 2, 200, "USD"),
                                new MessageReceiptElement("Third item", "Item description", "", 3, 1800, "USD"),
                            ],
                            'address' => new Address([
                                'country' => 'US',
                                'state' => 'CA',
                                'postal_code' => 94025,
                                'city' => 'Menlo Park',
                                'street_1' => '1 Hacker Way',
                                'street_2' => ''
                            ]),
                            'summary' => new Summary([
                                'subtotal' => 2300,
                                'shipping_cost' => 150,
                                'total_tax' => 50,
                                'total_cost' => 2500,
                            ]),
                            'adjustments' => [
                                new Adjustment([
                                    'name' => 'New Customer Discount',
                                    'amount' => 20
                                ]),

                                new Adjustment([
                                    'name' => '$10 Off Coupon',
                                    'amount' => 10
                                ])
                            ]
                        ]
                    ));
                    break;

                // When bot receive "set menu"
                case 'set menu':
                    $bot->deletePersistentMenu();
                    $bot->setPersistentMenu([
                        new LocalizedMenu('default', false, [
                            new MenuItem(MenuItem::TYPE_NESTED, 'My Account', [
                                new MenuItem(MenuItem::TYPE_NESTED, 'History', [
                                    new MenuItem(MenuItem::TYPE_POSTBACK, 'History Old', 'HISTORY_OLD_PAYLOAD'),
                                    new MenuItem(MenuItem::TYPE_POSTBACK, 'History New', 'HISTORY_NEW_PAYLOAD')
                                ]),
                                new MenuItem(MenuItem::TYPE_POSTBACK, 'Contact Info', 'CONTACT_INFO_PAYLOAD')
                            ])
                        ])
                    ]);
                    break;

                // When bot receive "delete menu"
                case 'delete menu':
                    $bot->deletePersistentMenu();
                    break;

                // When bot receive "login"
                case 'login':
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_GENERIC,
                        [
                            'elements' => [
                                new AccountLink(
                                    'Welcome to Bank',
                                    'To be sure, everything is safe, you have to login to your administration.',
                                    'https://www.example.com/oauth/authorize',
                                    'https://www.facebook.com/images/fb_icon_325x325.png')
                            ]
                        ]
                    ));
                    break;

                // When bot receive "logout"
                case 'logout':
                    $bot->send(new StructuredMessage($message['sender']['id'],
                        StructuredMessage::TYPE_GENERIC,
                        [
                            'elements' => [
                                new AccountLink(
                                    'Welcome to Bank',
                                    'To be sure, everything is safe, you have to login to your administration.',
                                    '',
                                    'https://www.facebook.com/images/fb_icon_325x325.png',
                                    TRUE)
                            ]
                        ]
                    ));
                    break;

                // When bot receive "sender action on"
                case 'sender action on':
                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_ON));
                    break;

                // When bot receive "sender action off"
                case 'sender action off':
                    $bot->send(new SenderAction($message['sender']['id'], SenderAction::ACTION_TYPING_OFF));
                    break;

                // When bot receive "set get started button"
                case 'set get started button':
                    $bot->setGetStartedButton('PAYLOAD - get started button');
                    break;

                // When bot receive "delete get started button"
                case 'delete get started button':
                    $bot->deleteGetStartedButton();
                    break;

                // When bot receive "show greeting text"
                case 'show greeting text':
                    $response = $bot->getGreetingText();
                    $text = "";
                    if(isset($response['data'][0]['greeting']) AND is_array($response['data'][0]['greeting'])){
                        foreach ($response['data'][0]['greeting'] as $greeting)
                        {
                            $text .= $greeting['locale']. ": ".$greeting['text']."\n";
                        }
                    } else {
                        $text = "Greeting text not set!";
                    }
                    $bot->send(new Message($message['sender']['id'], $text));
                    break;

                // When bot receive "delete greeting text"
                case 'delete greeting text':
                    $bot->deleteGreetingText();
                    break;

                // When bot receive "set greeting text"
                case 'set greeting text':
                    $bot->setGreetingText([
                        [
                            "locale" => "default",
                            "text" => "Hello {{user_full_name}}"
                        ],
                        [
                            "locale" => "en_US",
                            "text" => "Hi {{user_first_name}}, welcome to this bot."
                        ],
                        [
                            "locale" => "de_DE",
                            "text" => "Hallo {{user_first_name}}, herzlich willkommen."
                        ]
                    ]);
                    break;

                // When bot receive "set target audience"
                case 'show target audience':
                    $response = $bot->getTargetAudience();
                    break;

                // When bot receive "set target audience"
                case 'set target audience':
                    $bot->setTargetAudience("all");
                    //$bot->setTargetAudience("none");
                    //$bot->setTargetAudience("custom", "whitelist", ["US", "CA"]);
                    //$bot->setTargetAudience("custom", "blacklist", ["US", "CA"]);
                    break;

                // When bot receive "delete target audience"
                case 'delete target audience':
                    $bot->deleteTargetAudience();
                    break;

                // When bot receive "show domain whitelist"
                case 'show domain whitelist':
                    $response = $bot->getDomainWhitelist();
                    $text = "";
                    if(isset($response['data'][0]['whitelisted_domains']) AND is_array($response['data'][0]['whitelisted_domains'])){
                        foreach ($response['data'][0]['whitelisted_domains'] as $domains)
                        {
                            $text .= $domains."\n";
                        }
                    } else {
                        $text = "No domains in whitelist!";
                    }
                    $bot->send(new Message($message['sender']['id'], $text));
                    break;

                // When bot receive "set domain whitelist"
                case 'set domain whitelist':
                    //$bot->setDomainWhitelist("https://petersfancyapparel.com");
                    $bot->setDomainWhitelist([
                        "https://petersfancyapparel-1.com",
                        "https://petersfancyapparel-2.com",
                    ]);
                    break;

                // When bot receive "delete domain whitelist"
                case 'delete domain whitelist':
                    $bot->deleteDomainWhitelist();
                    break;

                // Other message received
                default:
                    if (!empty($command)) // otherwise "empty message" wont be understood either
                        $bot->send(new Message($message['sender']['id'], 'Sorry. I don’t understand you.'));
            }
        }
    }
}