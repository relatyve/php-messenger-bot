<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 2017.10.05.
 * Time: 19:42
 */

// fifth harmony"

// Mintagomba pimax

//$servername = "localhost";
//$username = "root";
//$password = "fender";
//$dbname = "bot";

require_once "db_config.php";

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$log_session = rand(100000,999999);

$verify_token = "rewtwre432wer21efqwe"; // Verify token
$token = "EAAEzpZBHw9MMBAKffHHZBZBZBcIuij7gNwGRz6OrWANNsAbcOvG3XVZByukZBBZCsRgj0B49oa3lv3E24rSc8ScPqIBz9ttloZAmi8KzyTo27FpxNkJjENy23DTlZATPo9Tr3HSTSROO125tCWeAnpQZA5XMb8X0GYx1DfWlSVLA4oLgZDZD"; // Page token



if (file_exists(__DIR__ . '/config.php'))
{
    $config = include __DIR__ . '/config.php';
    $verify_token = $config['verify_token'];
    $token = $config['token'];
}

require_once(dirname(__FILE__) . '/vendor/autoload.php');

use pimax\FbBotApp;
use pimax\Menu\MenuItem;
use pimax\Menu\LocalizedMenu;
use pimax\Messages\Message;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\MessageReceiptElement;
use pimax\Messages\Address;
use pimax\Messages\Summary;
use pimax\Messages\Adjustment;
use pimax\Messages\AccountLink;
use pimax\Messages\ImageMessage;
use pimax\Messages\QuickReply;
use pimax\Messages\QuickReplyButton;
use pimax\Messages\SenderAction;
use pimax\Messages\FileMessage;
use pimax\Messages\AudioMessage;
use pimax\Messages\VideoMessage;
use pimax\Messages\Attachment;
use pimax\Messages\GreetengMessage;

function db_logging($conn, $data, $mtype, $m, $q, $sess)
    {
    $stmt = $conn->prepare("INSERT INTO log (content, date, owner, messagetype, message, querytype, session) VALUES (:content, :date, :owner, :messagetype, :message, :querytype, :session)");
    $stmt->bindParam(':content', $content);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':owner', $owner);
    $stmt->bindParam(':messagetype', $messagetype);
    $stmt->bindParam(':message', $message);
    $stmt->bindParam(':querytype', $querytype);
    $stmt->bindParam(':session', $session);

    // insert a row
    $content = $data;
    $date = date("Y-m-d H:i:s");
    $owner = "-";
    $messagetype = $mtype;
    $message = $m;
    $querytype = $q;
    $session = $sess;
    $stmt->execute();
    }

function getUserData($access_token, $user_id)
    {
    $url = "https://graph.facebook.com/v2.6/".$user_id."?fields=first_name,last_name&access_token=" . $access_token;
    $headers = ['Content-Type: application/json',];
    $process = curl_init($url);

    curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($process, CURLOPT_HEADER, false);
    curl_setopt($process, CURLOPT_TIMEOUT, 30);

    curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
    $return = curl_exec($process);
    curl_close($process);

    return json_decode($return, true);
    }

function abstractMessage($botm, $sm, $token, $conn, $log_session)
    {
    $getmessage = $sm->getData();
    $getmessage['access_token'] = $token;

        db_logging($conn, json_encode($getmessage), "send", "-", "response", $log_session);

    $botm->send($sm);
    }

// Make Bot Instance
$bot = new FbBotApp($token);


if (!empty($_REQUEST['local']))
    {

    $message = new ImageMessage(1585388421775947, dirname(__FILE__).'/fb4d_logo-2x.png');
    $message_data = $message->getData();
    $message_data['message']['attachment']['payload']['url'] = 'fb4d_logo-2x.png';
    echo '<pre>', print_r($message->getData()), '</pre>';
    $res = $bot->send($message);
    echo '<pre>', print_r($res), '</pre>';
    }

// Receive something
if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token)
    {
    // Webhook setup request
    echo $_REQUEST['hub_challenge'];
    }
else
    {
    // Other event
    $data_raw = file_get_contents("php://input");

    $data = json_decode($data_raw, true, 512, JSON_BIGINT_AS_STRING);



    if (!empty($data['entry'][0]['messaging']))
        {

        // ******************** Sender ID **************
        $sender_id = $data['entry'][0]['messaging'][0]['sender']['id'];
        // ******************** Sender ID **************

        $array_userData = getUserData($token, $sender_id);

        // ++++++++++++++++++++++++++++++++++++++++++++++ welcome block ++++++++++++++++++++++++++++++++++++++++++++++++
        // ---------------- welcome image ------------------
        $welcomeImage = new ImageMessage($sender_id, 'https://itdevline.hu/messengerchatbot/demo/img/robot_0.jpg');
        // ---------------- welcome image end --------------
        // ---------------- welcome text ------------------
        $wmsg = ($array_userData['first_name'] ? "Szia " . $array_userData['first_name'] : 'Kedves Felhasználó')."! \n" .
                 "De örülök, hogy rám találtál! :-) Kicsit kezdtem már unatkozni... \n\n" .
                 "Én egy AUTOMATIZÁLT beszélgető robot vagyok:-)  Az Artificial Intelligence /Mesterséges Intelligencia/ és a Chatbot technológiával működök." .
                 "Itt és most, csak pár funkcióm működik, nem tudok mindenre válaszolni. Arra tudok válaszolni, amit megtanítottak nekem. :-) \n\n" .
                 "Viszont ha megtanítanak,  el tudok mondani mindent a cégedről, termékeidről, szolgáltatásaidról a vevőidnek.";
        $welcomeMessage = new Message($sender_id, $wmsg);
        // ---------------- welcome text ------------------

        // ---------------- images and button (generic) --------------------
        $welcomeGeneric = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_GENERIC,
                [
                'elements' =>
                    [
                    new MessageElement("További részletek", "Teszteld tovább!", "https://itdevline.hu/messengerchatbot/demo/img/nyil.jpg",
                        [
                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Folytassuk!', "FOLYTASSUK")
                        ]),

                    new MessageElement("Nem érdekelnek a további részletek", "", "https://itdevline.hu/messengerchatbot/demo/img/no.jpg",
                        [
                        new MessageButton(MessageButton::TYPE_POSTBACK, 'Ne folytassuk!', "NE_FOLYTASSUK")
                        ]),

                    new MessageElement("A készítő telefonos elérhetősége", "Ha vele szeretnél beszélni, kérdéseid vannak, hívd fel őt!", "https://itdevline.hu/messengerchatbot/demo/img/phone.jpg",
                        [
                            new MessageButton(MessageButton::TYPE_CALL, 'Telefonszám', '36204002544')
                        ])
                    ]
                ]
            );
        // ---------------- images and button end(generic) -----------------
        // ++++++++++++++++++++++++++++++++++++++++++++++ welcome block end +++++++++++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ GOODBYE block +++++++++++++++++++++++++++++++++++++++++++++++++

        $goodbye_button = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => "Rendben. Ha bármilyen kérdésed  vagy bármilyen elképzelésed van az együttműködésünről, hívd a készítőmet bizalommal, szívesen segítünk!\n\n" .
                "További szép napot neked!",
                'buttons' => [
                    new MessageButton(MessageButton::TYPE_CALL, 'Hívom most!', '36204002544')
                ]
            ]
        );
        $goodbyeImage = new ImageMessage($sender_id, 'https://itdevline.hu/messengerchatbot/demo/img/goodbye.jpg');

        // ++++++++++++++++++++++++++++++++++++++++++++++ GOODBYE block end +++++++++++++++++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ FOLYTASSUK block +++++++++++++++++++++++++++++++++++++++++++++++++
        // ---------------- folytassuk image ------------------
        $folytassukImage = new ImageMessage($sender_id, 'https://itdevline.hu/messengerchatbot/demo/img/kez.jpg');
        // ---------------- folytassuk image end --------------
        // ---------------- folytassuk text ------------------
        $fmsg = "A működésem nem bonyolult, de egy biztos: mint minden kollégának, nekem is meg kell ismernem az adott céget, az elvárásait, a kommunikációs stílust. Megtanították nekem, mit szabad, mit kötelező, mit tilos.\n\n".
        "Cserébe, hibátlanul dolgozom. Sosem esek ki a szerepemből. Nem megyek szabadságra, nem kések el, nem betegszem meg és akár hajnali 2 órakor is pontosan olyan kedvességgel válaszolok a kérdésekre, mint napközben." .
        "Pont úgy, ahogy most beszélgetek veled :-D";
        $folytassukMessage = new Message($sender_id, $fmsg);
        // ---------------- folytassuk text ------------------

       // ---------------- images and button (generic) --------------------
        $folytassukGeneric = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_GENERIC,
            [
                'elements' =>
                    [
                        new MessageElement("Tesztelj tovább!", "Nézd meg, milyen előnyökhöz jutsz vállalkozásodban, ha van ilyen GuestBot-od.", "https://itdevline.hu/messengerchatbot/demo/img/test_next.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Előnyök', "ELONYOK")
                            ]),

                        new MessageElement("Nem érdekel!", "Nem érdekelnek az előnyök", "https://itdevline.hu/messengerchatbot/demo/img/nyil.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Nem érdekel', "NE_FOLYTASSUK")
                            ])

                    ]
            ]
        );
        // ---------------- images and button end(generic) -----------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ FOLYTASSUK block end +++++++++++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ ELONYOK block +++++++++++++++++++++++++++++++++++++++++++++

        // ---------------- elonyok text ------------------
        $emsg = "- erősítem a márkahűséget\n" .
                "- profilozom a vevőidet\n" .
                "- összegzem a visszajelzéseiket\n" .
                "- erősítem a közösségi platformokat\n" .
                "- értékeléseket kérhetek különböző oldalakra\n" .
                "- konkrét, mérhető marketingadatokat biztosítok\n" .
                "- egy időben párhuzamosan akár az összes vevőddel tudok kommunikálni\n" .
                "- látom, ki kapcsolódott és miért\n" .
                "- aki beszélt már velem, annak bármikor Messenger üzenet küldhető\n\n" .

                "... a többit még nem árulom el:-)\n\n" .

                "Most bemutatom Neked egy nem létező étterem GuestBot-ját, hogy lásd műkődés közben a társamat. Ő is nagyon profi!\n" .
                "Persze nem kell étterem, bármilyen tevékenységú cég, aki terméket ad el vagy szolgáltat.";
        $elonyokMessage = new Message($sender_id, $emsg);
        // ---------------- elonyok text ------------------

        // ---------------- images and button (generic) --------------------
        $elonyokGeneric = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_GENERIC,
            [
                'elements' =>
                    [
                        new MessageElement("Éttermi GuestBot inas", "Csak pár funkciót mutat be /DEMO/", "https://itdevline.hu/messengerchatbot/demo/img/inas.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Folytassuk vele!', "ETTERMI_INAS")
                            ]),

                        new MessageElement("Nem érdekel az inas!!!", "", "https://itdevline.hu/messengerchatbot/demo/img/no.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Nem érdekel', "NE_FOLYTASSUK")
                            ])

                    ]
            ]
        );
        // ---------------- images and button end(generic) -----------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ ELONYOK block end +++++++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ ETTERMI INAS block ++++++++++++++++++++++++++++++++++++++++

        // ---------------- ettermi inas image ------------------
        $etterminasImage = new ImageMessage($sender_id, 'https://itdevline.hu/messengerchatbot/demo/img/inas2.jpg');
        // ---------------- ettermi inas image end --------------

        // ---------------- ettermi inas text ------------------
        $eimsg = "Szia! Üdvözöllek AZ Étteremben! Na jó, most éppen ücsörgök, de ilyenkor is megállás nélkül robotolok.\n" .
                "Tudok akár asztalt foglalni Vendégeidnek, megmutom az étlapot, bemutatom az éttermet!";
        $etterminasMessage = new Message($sender_id, $eimsg);
        // ---------------- ettermi inas text ------------------

        // ---------------- images and button (generic) --------------------
        $etterminasGeneric = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_GENERIC,
            [
                'elements' =>
                    [
                        new MessageElement("Galéria", "Képek az ételkínálatunkból", "https://itdevline.hu/messengerchatbot/demo/img/galeria.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'További ajánlatok', "GALERIA")
                            ]),
                        new MessageElement("Asztalfoglalás", "Ez csak egy DEMO, nyugodtan próbáld ki!", "https://itdevline.hu/messengerchatbot/demo/img/reverse.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'asztalfoglalás PRÓBA', "ASZTALFOGLALAS")
                            ]),
                        new MessageElement("Tartsd nálunk az esküvői mulatságot! Mutatom a helyszínt!", "", "https://itdevline.hu/messengerchatbot/demo/img/etterem.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Rendezvényterem', "RENDEZVENYTEREM")
                            ]),
                        new MessageElement("Megmondom a Vendégeidnek a Wi-Fi kódot", "", "https://itdevline.hu/messengerchatbot/demo/img/wifi.jpg",
                            [
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Wi-Fi', "WIFI")
                            ])

                    ]
            ]
        );
        // ---------------- images and button end(generic) -----------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ ETTERMI INAS block end ++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ GALERIA block +++++++++++++++++++++++++++++++++++++++++++++
        // ---------------- galeria1 text ------------------
        $galeriamsg1 = "Igen ez a sok szép kép mind nálunk készült! :)";
        $galeriaMessage1 = new Message($sender_id, $galeriamsg1);
        // ---------------- galeria1 text ------------------

        // ---------------- images and button (generic) --------------------
        $galeriaGeneric = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_GENERIC,
            [
                'elements' =>
                    [
                        new MessageElement("01", "", "https://itdevline.hu/messengerchatbot/demo/img/galeria_1.jpg"),
                        new MessageElement("02", "", "https://itdevline.hu/messengerchatbot/demo/img/galeria_2.jpg"),
                        new MessageElement("03", "", "https://itdevline.hu/messengerchatbot/demo/img/galeria_3.jpg"),
                        new MessageElement("04", "", "https://itdevline.hu/messengerchatbot/demo/img/galeria_4.jpg")

                    ]
            ]
        );
        // ---------------- images and button end(generic) -----------------

        // ---------------------------- menu -------------------------------
        $galeriaMenu = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => "Megmondjam neked, hogy mi  a napi menü, vagy a inkább a séf ajánlatát mutassam meg?",
                'buttons' => [
                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Napi menü', 'NAPI_MENU'),
                    new MessageButton(MessageButton::TYPE_POSTBACK, 'A séf ajánlata', 'SEF_AJANLATA'),
                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Köszönöm, most nem', 'NE_FOLYTASSUK')
                ]
            ]
        );
        // ---------------------------- menu end ---------------------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ GALERIA block end +++++++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ NAPI_MENU block +++++++++++++++++++++++++++++++++++++++++++
        // ---------------- napiMenu text ------------------
        $menumsg = "Ma ezt főzte a szakácsunk ebédre:\n" .
                    "- Harcsabajusz leves: 650 Ft\n" .
                    "- Mákos gubanc: 450 Ft\n" .
                    "- Rakott krumpli: 765 Ft\n\n" .
                    "Jó étvágyat!";
        $menuMessage = new Message($sender_id, $menumsg);
        // ---------------- napiMenu text ------------------

        // ---------------- images and button (generic) --------------------
        $menuGeneric = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_GENERIC,
            [
                'elements' =>
                    [
                        new MessageElement("Rántott pekenyóca", "- Nagyon finom, hidd el nekünk!", "https://itdevline.hu/messengerchatbot/demo/img/callcenter.jpg", "", "www.nincsilyen.hu"),
                        new MessageElement("Ez is az másképp", "- válogatott alapanyagokból", "https://itdevline.hu/messengerchatbot/demo/img/robot.jpg", "", "www.nincsilyen.hu"),
                        new MessageElement("Ez már nem az :-D", "ide már nem tudok mit írni :-)", "https://itdevline.hu/messengerchatbot/demo/img/inas2.jpg", "", "www.nincsilyen.hu")

                    ]
            ]
        );
        // ---------------- images and button end(generic) -----------------

        // ---------------------------- menu -------------------------------
        $menu1Menu = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
                [
                'text' => "Ugye, milyen jól dolgozom? Még gondolkodsz? Min?\nHívd a készítőt, legyen a Te cégednek is ilyen segítő robotja!",
                'buttons' => [new MessageButton(MessageButton::TYPE_CALL, 'Rendben, hívom', '36200000000')]
                ]
        );
        // ---------------------------- menu end ---------------------------
        // ---------------------------- menu -------------------------------
        $menu2Menu = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => "Megnézed a séf ajánlatát is?",
                'buttons' => [new MessageButton(MessageButton::TYPE_POSTBACK, 'A séf ajánlata', 'SEF_AJANLATA')]
            ]
        );
        // ---------------------------- menu end ---------------------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ NAPI_MENU block end +++++++++++++++++++++++++++++++++++++++


        // ++++++++++++++++++++++++++++++++++++++++++++++ SEF_AJANLATA block ++++++++++++++++++++++++++++++++++++++++
        // ---------------- sef 1 text ------------------
        $sefmsg = "Várj egy pillanatot, megkérdezem tőle!";
        $sefMessage = new Message($sender_id, $sefmsg);
        // ---------------- sef 1 text ------------------
        // ---------------------------- menu -------------------------------
        $sefMenu = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => "Ne haragudj, de sajnos nem találom, hogy hol van a séf 😞 Inkább foglalj asztalt nálunk és győződj meg róla személyesen, hogy milyen remekül főz!",
                'buttons' => [new MessageButton(MessageButton::TYPE_POSTBACK, 'Foglalj asztalt', 'ASZTALFOGLALAS')]
            ]
        );
        // ---------------------------- menu end ---------------------------
        // ---------------- sef image ------------------
        $sefImage = new ImageMessage($sender_id, 'https://itdevline.hu/messengerchatbot/demo/img/etel00.jpg');
        // ---------------- sef image end --------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ SEF_AJANLATA block end ++++++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ ASZTALFOGALALAS block +++++++++++++++++++++++++++++++++++++++++
        // ---------------------------- menu -------------------------------
        $asztalfoglalasMenu = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => "Ez csak egy próba. Sajnos itt most nem tudsz asztalt foglalni! Csak mutatom neked, hogy szinte nincs olyan, amit nem tudok :)\n\n" .
                            "Navigálni, asztal foglalni, terméket eladni... Nem is tudom mindet felsorolni.\n\n" .
                            "Ugye, milyen jól dolgozom? Még gondolkodsz? Min?\n" .
                            "Hívd a készítőt, legyen a Te cégednek is ilyen segítő robotja!",
                'buttons' => [
                                new MessageButton(MessageButton::TYPE_CALL, 'Hívom azonnal', '36300000000'),
                                new MessageButton(MessageButton::TYPE_POSTBACK, 'Vagy búcsúzol tőlem?', 'NE_FOLYTASSUK'),
                            ]
            ]
        );
        // ---------------------------- menu end ---------------------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ ASZTALFOGALALAS block +++++++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ RENDEZVENYTEREM block +++++++++++++++++++++++++++++++++++++++++
        // ---------------------------- menu -------------------------------
        $rendezvenyMenu = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => "Tudjuk, hogy az esküvő különleges és egyedi, ezért fokozott figyelemmel dolgozunk, hogy felejthetetlenné tegyük ezt a napot számodra.\n\n" .
                            "Célunk, hogy levegyük a terhet a válladról és teljes körű szolgáltatást nyújtsunk a tervezéstől a megrendezésig.",
                'buttons' => [
                    new MessageButton(MessageButton::TYPE_CALL, 'Foglald le most!', '36300000000')
                ]
            ]
        );
        // ---------------------------- menu end ---------------------------
        // ---------------- rendezvenyterem image ------------------
        $rendezvenyteremImage = new ImageMessage($sender_id, 'https://itdevline.hu/messengerchatbot/demo/img/etterem.jpg');
        // ---------------- rendezvenyterem image end --------------

        // ++++++++++++++++++++++++++++++++++++++++++++++ RENDEZVENYTEREM block end +++++++++++++++++++++++++++++++++++++

        // ++++++++++++++++++++++++++++++++++++++++++++++ WIFI block ++++++++++++++++++++++++++++++++++++++++++++++++++++
        $wifiMenu = new StructuredMessage($sender_id,
            StructuredMessage::TYPE_BUTTON,
            [
                'text' => "Név: wifiname \nJelszó: 123456789",
                'buttons' => [
                    new MessageButton(MessageButton::TYPE_CALL, 'Szeretnék GuestBotot', '36300000000')
                ]
            ]
        );
        // ---------------------------- menu end ---------------------------
        // ++++++++++++++++++++++++++++++++++++++++++++++ WIFI block end ++++++++++++++++++++++++++++++++++++++++++++++++


        // ++++++++++++++++++++++++++++++++++++++++++++++ String default response +++++++++++++++++++++++++++++++++++++++
            $defaultmsg = "Sajnálom, nem ismerem fel amit írtál... Egyenlőre nem tanítottak még semmire sem...";
            $defaultMessage = new Message($sender_id, $defaultmsg);
        // ++++++++++++++++++++++++++++++++++++++++++++++ String default response +++++++++++++++++++++++++++++++++++++++


            // **********************************************************************************************
        // ***************************************** CONTENTS *******************************************
        // **********************************************************************************************


        foreach ($data['entry'][0]['messaging'] as $message)
            {
            // Skipping delivery messages
            if (!empty($message['delivery']))
                {
                continue;
                }


            $command = "";

            // When bot receive message from user
            if (!empty($message['message']))
                {
                $command = trim($message['message']['text']);

                db_logging($conn, $data_raw, "text", $command, "request", $log_session);


                    // Handle command
                    switch ($command)
                        {
                        // Other message received
                        default:
                            if (!empty($command))
                                {
                                abstractMessage($bot, $defaultMessage, $token, $conn, $log_session);
                                // $bot->send(new Message($message['sender']['id'], "Sajnálom, nem ismerem fel amit írtál... Egyenlőre nem tanítottak még semmire sem..."));
                                }
                        }

                }

            else if (!empty($message['postback']))
                {
                    // ***************************************************************************
                    // ******************************* BUTTON SECTION ****************************
                    // ***************************************************************************

                db_logging($conn, $data_raw, "postback", $message['postback']['payload'], "request", $log_session);


                switch ($message['postback']['payload'])
                    {
                    case 'GET_STARTED_BUTTON':
                        abstractMessage($bot, $welcomeImage, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $welcomeMessage, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $welcomeGeneric, $token, $conn, $log_session);
                        break;

                    case 'NE_FOLYTASSUK':
                        abstractMessage($bot, $goodbye_button, $token, $conn, $log_session);
                        abstractMessage($bot, $goodbyeImage, $token, $conn, $log_session);
                        break;

                    case 'FOLYTASSUK':
                        abstractMessage($bot, $folytassukImage, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $folytassukMessage, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $folytassukGeneric, $token, $conn, $log_session);
                        break;

                    case 'ELONYOK':
                        abstractMessage($bot, $elonyokMessage, $token, $conn, $log_session);
                        abstractMessage($bot, $elonyokGeneric, $token, $conn, $log_session);
                        break;

                    case "ETTERMI_INAS":
                        abstractMessage($bot, $etterminasImage, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $etterminasMessage, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $etterminasGeneric, $token, $conn, $log_session);
                        break;

                    case "GALERIA":
                        abstractMessage($bot, $galeriaMessage1, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $galeriaGeneric, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $galeriaMenu, $token, $conn, $log_session);
                        break;

                    case "NAPI_MENU":
                        abstractMessage($bot, $menuMessage, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $menuGeneric, $token, $conn, $log_session);
//                        sleep(1);
                        abstractMessage($bot, $menu1Menu, $token, $conn, $log_session);
                        abstractMessage($bot, $menu2Menu, $token, $conn, $log_session);
                        break;

                    case "SEF_AJANLATA":
                        abstractMessage($bot, $sefMessage, $token, $conn, $log_session);
                        abstractMessage($bot, $sefMenu, $token, $conn, $log_session);
                        abstractMessage($bot, $sefImage, $token, $conn, $log_session);
                        break;

                    case "ASZTALFOGLALAS":
                        abstractMessage($bot, $asztalfoglalasMenu, $token, $conn, $log_session);
                        break;

                    case "RENDEZVENYTEREM":
                        abstractMessage($bot, $rendezvenyMenu, $token, $conn, $log_session);
                        abstractMessage($bot, $rendezvenyteremImage, $token, $conn, $log_session);
                        break;

                    case "WIFI":
                        abstractMessage($bot, $wifiMenu, $token, $conn, $log_session);
                        break;
                    }

                continue;
            }
            // ***************************************************************************
            // ******************************* BUTTON SECTION END ************************
            // ***************************************************************************


        }
    }
}